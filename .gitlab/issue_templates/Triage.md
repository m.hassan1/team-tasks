Process:

- [Triage process](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/triage.html)
- [Frequently used labels](https://gitlab.com/gitlab-org/distribution/team-tasks/-/blob/master/frequently-used-labels.md)

Links:

- [Omnibus issues to triage](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For%20Scheduling&not%5Blabel_name%5D%5B%5D=awaiting%20feedback&not%5Blabel_name%5D%5B%5D=needs%20investigation&not%5Blabel_name%5D%5B%5D=pipeline%20failure&scope=all&sort=created_date&state=opened&not%5Btype%5D%5B%5D=task)
- [Charts issues to triage](https://gitlab.com/gitlab-org/charts/gitlab/-/issues/?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For%20Scheduling&not%5Blabel_name%5D%5B%5D=awaiting%20feedback&not%5Blabel_name%5D%5B%5D=needs%20investigation&not%5Blabel_name%5D%5B%5D=FedRAMP%20Milestone%3A%3A%2a&not%5Blabel_name%5D%5B%5D=pipeline%20failure&scope=all&sort=created_date&state=opened&not%5Btype%5D%5B%5D=task)
- [Operator issues to triage](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For+Scheduling&not%5Blabel_name%5D%5B%5D=awaiting+feedback&not%5Blabel_name%5D%5B%5D=needs+investigation&scope=all&sort=created_date&state=opened&not[type][]=task&not[label_name][]=pipeline+failure)
- Omnibus Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=None&label_name%5B%5D=awaiting+feedback&milestone_title=None&page=3&scope=all&sort=updated_desc&state=opened&not[type][]=task) (start from last page)
- Chart Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/charts/gitlab/issues?label_name%5B%5D=awaiting+feedback&milestone_title=None&scope=all&state=opened&not[type][]=task) (start from last page)
- Operator Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/issues?label_name%5B%5D=awaiting+feedback&milestone_title=None&scope=all&state=opened&not[type][]=task) (start from last page)
- [Omnibus pipeline failure issue board](https://gitlab.com/gitlab-org/omnibus-gitlab/-/boards/3211630?label_name[]=pipeline%20failure)
- [CNG pipeline failure issue board](https://gitlab.com/gitlab-org/charts/gitlab/-/boards/3503121?label_name[]=pipeline%20failure)
- [Operator pipeline failure issue board](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/boards/3091865?label_name[]=pipeline%20failure)
- ~Triage [issue history](https://gitlab.com/gitlab-org/distribution/team-tasks/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=Triage)

/label ~Triage ~"group::distribution" ~"devops::systems" ~"section::enablement"
/due next friday
