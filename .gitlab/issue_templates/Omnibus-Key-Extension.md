# Omnibus Package Signing Key Extension

The Omnibus package signing key (not to be confused with the repository GPG key
used to sign metadata for `yum` or `apt`) is set to a yearly expiration time and
needs to be extended for another year. Because the key is used to sign the
content of the Omnibus packages within the pipe line, it has become the
Distribution Team's best practice to extend the expiration of the key rather
than rotate the key. This minimizes disruption both within GitLab and for users
that use the public key to validate the package signatures.

## TODO list for extending the key so that can insure that all items are completed:

* Blog post
  - [ ] Create Corporate [Marketing announcement request][] issue
  - [ ] Create [blog issue][] & MR with the blog content
  - [ ] Publish blog post about expiration time extension
* Extend package key expiration time
  - [ ] Verify access to S3 bucket in AWS Gitlab.com account
  - [ ] Extend expiration of Omnibus package signing key [GitLab runbook][]
  - [ ] Backup original secret key in the omnibus-sig bucket
  - [ ] Upload secret key to the omnibus-sig bucket
  - [ ] Eradicate secret key from local keyring and disk
  - [ ] Upload public key to [keyservers](https://keys.openpgp.org)
  - [ ] Upload public key to package.gitlab.com repos
    - [ ] gitlab-ce
    - [ ] gitlab-ee
    - [ ] unstable
    - [ ] raspberry-pi2
    - [ ] gitlab-ce
    - [ ] pre-release
  - [ ] Verify that new public key is properly populated in CloudFront cache

It can take a couple of days to work through the process for publishing a blog
post. Plan accordingly and plan on having the blog published at least a week
before the old expiration time.

If you don't have a AWS Gitlab.com account--separate from the GitlabTop account
which is the normal account for Distribution Engineers--then an [access request][]
will need to be submitted to have the account created. Insure that when the
request is made that you specify that write access is needed to the `omnibus-sig`
bucket.

It has been observed that the CloudFront cache can respond differently depending
upon whether the user-agent is a browser or command line utility like `curl`.
(Note: `curl` requires that `-L` be specified so that the HTTP redirect is
followed) When verifying that the public key has been properly updated in each repo, both
methods should be validated. If either method fails to return the correct key,
then the following command can be used to invalidate the cache:

```shell
aws cloudfront create-invalidation --distribution-id <distribution-id> --paths <file-path>
```

The `file-path` can best be found by looking at the `Location` header returned
by `curl -v`. The hostname and query parameter are dropped from the `Location`
value and the resulting path has the form of `/7/11/gpg/gitlab-gitlab-ee-3D645A26AB9FBD22.pub.gpg`.
The CloudFront cache for `package.gitlab.com` is in a separate account and it
may be necessary for an Omnibus maintainer to execute the above command to
clear the cache.

[GitLab runbook]: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/packaging/manage-package-signing-keys.md
[Marketing announcement request]: https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=announcement
[blog issue]: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new&issuable_template=blog-post
[access request]: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Access_Change_Request

## Blog post text

The following text is used for the blog post. This text is the starting point
and needs to be updated each time it is posted. It is also important to
verify that all URL links resolve to the content that is to be expected. If a
link is found to be no longer valid, please update the team task issue template
with the corrected URL.

```markdown
---
title: "GitLab extends Omnibus package signing key expiration by one year"
author: "<AUTHOR NAME>"
author_gitlab: <AUTHOR GITLAB ID>
author_twitter: <AUTHOR TWITTER ID>
categories: news
image_title: "/images/default-blog-image.png"
description: "Our GPG key will now expire on July 1, <YEAR>. Here's what you need to know."
tags: security, security releases, workflow
twitter_text: "Why GitLab is extending the package signing key expiration by one year"
postType: product
---

GitLab uses a GPG key to sign all Omnibus packages created within the CI pipelines to insure that the packages have not been tampered with. This key is seperate from the repository metadata signing key used by package managers and the GPG signing key for the GitLab Runner. The Omnibus package signing key is set to expire on July 1, <YEAR> and will be extended to expire on July 1, <YEAR> instead.

## Why are we extending the deadline?

The Omnibus package signing key's expiration is extended each year to comply with GitLab security policies and to limit the exposure should the key become compromised. The key's expiration is extended instead of rotating to a new key to be less disruptive for users that do verify package integrity checks prior to installing the package.

## What do I need to do?

The only action that needs to be taken is to update your copy of the package signing key _if_ you validate the signatures on the Omnibus packages that GitLab distributes.

The package signing key is not the key that signs the repository metadata used by the OS package managers like `apt` or `yum.` Unless you are specifically verifying the package signatures or have configured your package manager to verify the package signatures, there is no action needed on your part to continue installing Omnibus packages.

More information concerning [verification of the package signatures](https://docs.gitlab.com/omnibus/update/package_signatures#package-signatures)
is available in the Omnibus documentation. If you just need to refresh a copy
of the public key, then you can find it on any of the GPG keyservers by
searching for support@gitlab.com or using the key ID of
`DBEF 8977 4DDB 9EB3 7D9F  C3A0 3CFC F9BA F27E AB47.` Alternatively you could
download it directly from packages.gitlab.com using the URL:

    https://packages.gitlab.com/gitlab/gitlab-ce/gpgkey/gitlab-gitlab-ce-3D645A26AB9FBD22.pub.gpg

## What do I do if I still have problems?

Please open an issue in the [omnibus-gitlab issue tracker](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/new?issue&issuable_template=Bug).
```

cc @gitlab-org/distribution

/label ~"devops::systems" ~"section::enablement" ~"group::distribution"
